FROM python:slim-buster
RUN pip install falcon gunicorn names
COPY ./api/api.py .
ENTRYPOINT ["gunicorn", "-b", "0.0.0.0:8080", "api:api"]
EXPOSE 8080

import json
import names
import falcon
import random
import string

MAX_USERS = 10
KEYS = ["first_name", "last_name", "password"]
USERS = []


def get_user(req, resp):
  for user in USERS:
    score = 0
    for param in req.params:
      if param in user.keys() and user[param] == req.params[param]:
        score += 1
    if score == len(req.params):
      resp.body = json.dumps(user)
      resp.status = falcon.HTTP_200
      return
  r = {"error": "unknown user"}
  resp.body = json.dumps(r)
  resp.status = falcon.HTTP_404


def post_user(req, resp):
  user = req.media.get("user")
  if user not in USERS:
    USERS.append(user)
    r = {"user": user, "method": req.method}
    resp.body = json.dumps(r)
    resp.status = falcon.HTTP_200
    return
  r = {"error": "user already exists"}
  resp.body = json.dumps(r)
  resp.status = falcon.HTTP_409


def patch_user(req, resp):
  user = req.media.get("user")
  if user not in USERS:
    USERS.append(user)
  r = {"user": user, "method": req.method}
  resp.body = json.dumps(r)
  resp.status = falcon.HTTP_200


def delete_user(req, resp):
  user = req.media.get("user")
  if user in USERS:
    USERS.remove(user)
    r = {"user": user, "method": req.method}
    resp.body = json.dumps(r)
    resp.status = falcon.HTTP_200
    return
  r = {"error": "unknown user"}
  resp.body = json.dumps(r)
  resp.status = falcon.HTTP_404


def check_post_params(req, resp):
  invalid_params = None
  if req.media.get("user") is not None:
    invalid_params = [param for param in req.media.get("user") if param not in KEYS]
  if invalid_params is None or len(invalid_params) > 0:
    r = {"invalid params": req.media}
    resp.body = json.dumps(r)
    resp.status = falcon.HTTP_400
    return False
  return True


def check_params(req, resp):
  invalid_params = [param for param in req.params if param not in KEYS]
  if len(invalid_params) > 0:
    r = {"invalid params": req.params}
    resp.body = json.dumps(r)
    resp.status = falcon.HTTP_400
    return False
  return True


class User:
  def __init__(self):
    self.first_name = None
    self.last_name = None
    self.password = None

  def on_get(self, req, resp):
    if len(req.query_string) == 0:
      r = {"users": USERS}
      resp.body = json.dumps(r)
    elif check_params(req, resp):
      get_user(req, resp)
    else:
      return
    resp.content_type = falcon.MEDIA_JSON

  def on_post(self, req, resp):
    if check_post_params(req, resp):
      post_user(req, resp)
    resp.content_type = falcon.MEDIA_JSON

  def on_delete(self, req, resp):
    if check_post_params(req, resp):
      delete_user(req, resp)
    resp.content_type = falcon.MEDIA_JSON

  def on_put(self, req, resp):
    if check_post_params(req, resp):
      patch_user(req, resp)
    resp.content_type = falcon.MEDIA_JSON

  def on_patch(self, req, resp):
    if check_post_params(req, resp):
      patch_user(req, resp)
    resp.content_type = falcon.MEDIA_JSON

  def to_dict(self):
    return {'first_name': self.first_name,
            'last_name': self.last_name,
            'password': self.password}

  def __str__(self):
    return str(self.to_dict())

  def __repr__(self):
    return self.__str__()


for user in range(0, MAX_USERS):
  user = User()
  user.first_name = names.get_first_name()
  user.last_name = names.get_last_name()
  user.password = ''.join(random.sample(string.ascii_lowercase + string.digits, 20))
  USERS.append(user.to_dict())

api = falcon.API()
api.add_route('/user', User())
